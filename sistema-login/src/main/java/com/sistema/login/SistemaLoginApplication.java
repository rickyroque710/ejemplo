package com.sistema.login;

import com.sistema.login.modelo.*;
import com.sistema.login.servicios.usuarioServicio;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemaLoginApplication implements CommandLineRunner{
        
    @Autowired
    private usuarioServicio usuarioServicio;
    
    public static void main(String[] args) {
            SpringApplication.run(SistemaLoginApplication.class, args);
    }

    
    @Override
	public void run(String... args) throws Exception {
		
                   
                    /*
                    //este codigo sirve para ingresar usuarios de tipo administrador al sistema
                    //en caso de no ser usado, mantener comentado este fragmento
                    
            
                    usuario usuario = new usuario();
                    usuario.setNombre("admin");
                    usuario.setUsername("admin@admin.com");
                    usuario.setPassword("12345678");
                    usuario.setPerfil("administrador");

                    rol rol = new rol();
                    rol.setRolId(1L);
                    rol.setNombre("admin");

                    Set<usuarioRol> usuariosRoles = new HashSet<>();
                    usuarioRol usuarioRol = new usuarioRol();
                    usuarioRol.setRol(rol);
                    usuarioRol.setUsuario(usuario);
                    usuariosRoles.add(usuarioRol);

                    usuario usuarioGuardado = usuarioServicio.guardarUsuario(usuario,usuariosRoles);
                    System.out.println(usuarioGuardado.getUsername());
            
                */
	}
        
}
