/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sistema.login.controlador;

import com.sistema.login.modelo.curso;
import com.sistema.login.servicios.cursoServicio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 *
 * @author Ricky Roque
 */
@RestController
@RequestMapping("/curso")
@CrossOrigin("*")
public class cursoControlador {
    
    @Autowired
    private cursoServicio cursoServicio;

    @PostMapping("/")
    public ResponseEntity<curso> guardarCurso(@RequestBody curso curso){
        curso cursoGuardado = cursoServicio.agregarCurso(curso);
        return ResponseEntity.ok(cursoGuardado);
    }

    @GetMapping("/{cursoId}")
    public curso listarCursoPorId(@PathVariable("cursoId") Long cursoId){
        return cursoServicio.obtenerCurso(cursoId);
    }

    @GetMapping("/")
    public ResponseEntity<?> listarCursos(){
        return ResponseEntity.ok(cursoServicio.obtenerCursos());
    }

    @PutMapping("/")
    public curso actualizarCurso(@RequestBody curso curso){
        return cursoServicio.actualizarCurso(curso);
    }

    @DeleteMapping("/{cursoId}")
    public void eliminarCurso(@PathVariable("cursoId") Long animalId){
        cursoServicio.eliminarCurso(animalId);
    }
}
