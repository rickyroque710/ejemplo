/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sistema.login.controlador;

import com.sistema.login.modelo.*;
import com.sistema.login.servicios.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import org.springframework.http.ResponseEntity;

import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author Ricky Roque
 */
@RestController
//declaracion de la url de acceso para el frontend
@RequestMapping("/usuarios")
@CrossOrigin("*")
public class usuarioControlador {
    
    @Autowired
    private usuarioServicio usuarioServicio;
    
    
    //metodo guardar, se analiza el tipo de usuario antes de guardar por medio de los if
    @PostMapping("/")
    public usuario guardarUsuario(@RequestBody usuario usuario) throws Exception{
        
        Set<usuarioRol> usuarioRoles = new HashSet<>();
        
        System.out.println(usuario.getNombre());
        rol rol = new rol();
        
        if(usuario.getNombre().equals("creador")){
            rol.setRolId(2L);
            rol.setNombre("creador");
        }
        if(usuario.getNombre().equals("consumidor") ){
            rol.setRolId(3L);
            rol.setNombre("consumidor");
        }
        
        usuarioRol usuarioRol = new usuarioRol();
        usuarioRol.setUsuario(usuario);
        usuarioRol.setRol(rol);

        usuarioRoles.add(usuarioRol);
        
        return usuarioServicio.guardarUsuario(usuario,usuarioRoles);
    }
 
    //obtener por nombre de usuario
    @GetMapping("/{username}")
    public usuario obtenerUsuario(@PathVariable("username") String username){
        return usuarioServicio.obtenerUsuario(username);
    }

    //borrar por id de usuario
    @DeleteMapping("/{usuarioId}")
    public void eliminarUsuario(@PathVariable("usuarioId") Long usuarioId){
        usuarioServicio.eliminarUsuario(usuarioId);
    }
    
    //lista de todos los usuarios
    @GetMapping("/")
    public ResponseEntity<?> listarUsuarios(){
        return ResponseEntity.ok(usuarioServicio.obtenerUsuarios());
    }
    
}
