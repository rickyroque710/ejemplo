/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sistema.login.repositorio;


import com.sistema.login.modelo.rol;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 *
 * @author Ricky Roque
 */
public interface rolRepositorio extends JpaRepository<rol,Long>{
    
}
