/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sistema.login.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Ricky Roque
 */
@Entity
@Table(name = "cursos")
public class curso {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long cursolId;

    private String titulo;
    private String estado;

    public curso() {
    }

    public curso(Long cursolId, String titulo, String estado) {
        this.cursolId = cursolId;
        this.titulo = titulo;
        this.estado = estado;
    }

    public Long getCursolId() {
        return cursolId;
    }

    public void setCursolId(Long cursolId) {
        this.cursolId = cursolId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
}
