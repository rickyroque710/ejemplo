/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sistema.login.servicios;

import com.sistema.login.modelo.curso;
import java.util.Set;

/**
 *
 * @author Ricky Roque
 */
public interface cursoServicio {
    curso agregarCurso(curso curso);

    curso actualizarCurso(curso curso);

    Set<curso> obtenerCursos();

    curso obtenerCurso(Long cursolId);
    
    void eliminarCurso(Long cursolId);
}
