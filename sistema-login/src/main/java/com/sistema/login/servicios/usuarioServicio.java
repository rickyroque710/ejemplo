/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.sistema.login.servicios;

import com.sistema.login.modelo.*;

import java.util.Set;
/**
 *
 * @author Ricky Roque
 */

public interface usuarioServicio {
    /**
     * declaracion de los servicios crud para el usuario
    */
    
    public usuario guardarUsuario(usuario usuario, Set<usuarioRol> usuarioRoles) throws Exception;
    
    public usuario obtenerUsuario(String username);

    public void eliminarUsuario(Long usuarioId);
    
    
    //todos los usuarios
    Set<usuario> obtenerUsuarios();
}
