/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.sistema.login.servicios.impl;

import com.sistema.login.modelo.curso;
import com.sistema.login.repositorio.cursoRepositorio;
import com.sistema.login.servicios.cursoServicio;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Ricky Roque
 */
@Service
public class CursoServiceImpl implements cursoServicio{
    
    @Autowired
    private cursoRepositorio cursoRepositorio;
    
    @Override
    public curso agregarCurso(curso curso) {
        return cursoRepositorio.save(curso);
    }

    @Override
    public curso actualizarCurso(curso curso) {
        return cursoRepositorio.save(curso);
    }

    @Override
    public Set<curso> obtenerCursos() {
        return new LinkedHashSet<>(cursoRepositorio.findAll());
    }

    @Override
    public curso obtenerCurso(Long cursoId) {
        return cursoRepositorio.findById(cursoId).get();
    }

    @Override
    public void eliminarCurso(Long cursoId) {
        curso curso = new curso();
        curso.setCursolId(cursoId);
        cursoRepositorio.delete(curso);
    }
    
}
